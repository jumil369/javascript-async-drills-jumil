 function fetchRandomNumbers() {
     return new Promise((resolve) => {
         console.log('Fetching number...');
         setTimeout(() => {
             let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
             resolve(randomNum);
         }, (Math.floor(Math.random() * (5)) + 1) * 1000)
     })
 }

 function fetchRandomString() {
     return new Promise((resolve) => {
         console.log('Fetching string...');
         setTimeout(() => {
             let result = '';
             let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
             let charactersLength = characters.length;
             for (let i = 0; i < 5; i++) {
                 result += characters.charAt(Math.floor(Math.random() * charactersLength));
             }
             resolve(result);

         }, (Math.floor(Math.random() * (5)) + 1) * 1000);
     });
 }

 //Task 1
 function partOne() {
     fetchRandomNumbers().then((randomNum) => {
         console.log(randomNum)
     });
     fetchRandomString().then((randomString) => {
         console.log(randomString)
     });

 }

 //Task 2
 async function partTwo() {
    let sum = await Promise.all([fetchRandomNumbers(),fetchRandomNumbers()])
     console.log("sum " + (parseInt(sum[0]) + parseInt(sum[1])));
 }

 //Task 3
 async function partThree() {
     let concatenate = await Promise.all([fetchRandomNumbers(),fetchRandomString()])
     console.log("After Concatenate : " + concatenate[0] + concatenate[1]);
 }

 //Task 4
 async function partFour() {
     let sum = 0;
     for (let i = 1; i <= 10; i++) {
         sum += parseInt(await fetchRandomNumbers());
     }
     console.log("sum " + sum);
 }

//  partOne();
 partTwo();
//  partThree();
//  partFour();