const fs = require("fs");
/**
NOTE: Do not use synchronous methods like readFileSync, writeFileSync for any of the tasks
      Do not use callbacks to implement, create your own promise based functions
 *
*/
/*
Task 1: Read the zomato.json file, choose a restaurant you like, and write it to
another file named my-fav-restaurant.json in output directory.
read zomato.json -> write task-1.json -> print 'Task 1 done'
*/


function getFavoriteRestaurant() {
    return new Promise((resolve, reject) => {
        const zomato = require('./data/zomato.json');
        zomato.restaurants.map((restaurant) => {
            if (restaurant.restaurant.name == "The Hudson Cafe") {
                const jsonString = JSON.stringify(restaurant.restaurant)
                fs.writeFile('./output/task-1.json', jsonString, err => {
                    if (err) {
                        reject("Task 1 not completed");
                    } else {
                        resolve("Task 1 done");
                    }
                });
            }
        })

    });
}
getFavoriteRestaurant().then(function(output) {
    console.log(output);
});

/*
Task 2: Read the movies.json file, choose a movie you like, and write it to
file named task-2-1.json in output directory, now read the zomato.json,
and write it to a file named task-2-2.json.
NOTE: You have to read the file twice, and it sequence should be
read movies.json -> write task-2-1.json -> read movies.json -> write task-2-2.json -> print 'Task 2 done'
*/
function getFavoriteMovies() {
    return new Promise((resolve, reject) => {
        const movies = require('./data/movies.json');
        movies.map((movie) => {
            if (movie.Title == "The Godfather") {
                const jsonString = JSON.stringify(movie)
                fs.writeFile('./output/task-2-1.json', jsonString, err => {
                    if (err) {
                        reject("Error found on task-2-1.json");
                    }
                })
            }
        });

        const zomato = require('./data/zomato.json');
        zomato.restaurants.map((restaurant) => {
            if (restaurant.restaurant.name == "The Hudson Cafe") {
                const jsonString = JSON.stringify(restaurant.restaurant)
                fs.writeFile('./output/task-2-2.json', jsonString, err => {
                    if (err) {
                        reject("Error found on task-2-2.json");
                    } else {
                        resolve("Task 2 done");
                    }
                });
            }
        });
    });
}
getFavoriteMovies().then(function(result) {
    console.log(result);
});
/*
Task 3: Read the movies.json file, choose 2 movies you like, and write it to
files named task-3-1.json and task-3-2.json.
NOTE: You have to read the file once, and simultaneously write to the two files
read movies.json -> task-3-1.json, write task-3-2.json -> print 'Task 3 done'
*/
function getFavoriteRestaurantAndMovies() {
    return new Promise((resolve, reject) => {
        const movies = require('./data/movies.json');
        let moviesList = [];
        movies.map((movie) => {
            if (movie.Title == "The Godfather" || movie.Title == "The Godfather: Part II") {
                moviesList.push(movie)
            }
        });
        const movieOne = JSON.stringify(moviesList[0])
        const movieTwo = JSON.stringify(moviesList[1])

        fs.writeFile('./output/task-3-1.json', movieOne, err => {
            if (err) {
                reject("Error found on task-3-1.json");
            }
        });
        fs.writeFile('./output/task-3-2.json', movieTwo, err => {
            if (err) {
                reject("Error found on task-3-2.json");
            }
        });
        resolve("Task 3 done");

    })
}
getFavoriteRestaurantAndMovies().then(function(val) {
    console.log(val);
});