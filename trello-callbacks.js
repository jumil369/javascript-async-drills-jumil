function getBoard(callback) {
    return setTimeout(function() {
        let board = {
            id: "def453ed",
            name: "Thanos"
        };
        callback(board);
    }, 1000);
}

function getLists(boardId, callback) {
    return setTimeout(function() {
        let lists = {
            def453ed: [{
                    id: "qwsa221",
                    name: "Mind"
                },
                {
                    id: "jwkh245",
                    name: "Space"
                },
                {
                    id: "azxs123",
                    name: "Soul"
                },
                {
                    id: "cffv432",
                    name: "Time"
                },
                {
                    id: "ghnb768",
                    name: "Power"
                },
                {
                    id: "isks839",
                    name: "Reality"
                }
            ]
        };
        callback(lists[boardId]);
    }, 1000);
}

function getCards(listId, callback) {
    return setTimeout(function() {
        let cards = {
            qwsa221: [{
                    id: "ornd494",
                    description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
                },
                {
                    id: "lwpw123",
                    description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
                }
            ]
        };
        callback(cards[listId]);
    }, 1000);
}
// Task 1 board -> lists -> cards for list qwsa221
getBoard((board) => {
    getLists(board.id, (list) => {
        let card = list.filter((card) => card.id == 'qwsa221');
        console.log("Card for qwsa221 \n id :" + card[0].id + " , " + "name :" + card[0].name);
    });

});

// Task 2 board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously
getBoard((board) => {
    getLists(board.id, (list) => {
        let result = [];
        list.map((card) => {
            if (card.id == 'qwsa221' || card.id == 'jwkh245') {
                result.push(card);
            }
        });
        setTimeout(() => {
            console.log("Cards for list qwsa221 and jwkh245");
            console.log(result[0], result[1]);
        }, 1000);
    });
});

// Task 3 board -> lists -> cards for all lists simultaneously
getBoard((board) => {
    getLists(board.id, (cards) => {
        setTimeout(() => {
            console.log("Cards List\n", cards);
        }, 1000);

    });
});